import os
import numpy as np
import matplotlib
import matplotlib.pyplot as plot

# Staðsetning gagnaskráa
directory = '/home/thg191/Documents/HI/V2019/Verklegt/Leiðniskömmtun/gallgull/'
# Skilgreinum breytur
V0 = 0.1011     # Spenna frá spennugjafa
R = 1000        # Viðnám
Vz = 0          # Skekkja í sveiflusjá
# Vz = -0.7e-3
Iz = Vz/R       # Skekkja í straumi
BW = 8e-7
G0 = 7.7498e-5


# Hjálparforrit til að telja línur í skrá 
def file_len(filename):
    count = 0
    f = open(filename)
    lines = f.readlines()
    for line in lines:
        count = count + 1
    return count

# Hjálparforrit til að telja csv skrár í möppu
def file_num(directory):
    files = 0
    for filename in os.listdir(directory):
        if filename.endswith('.csv'):
            files = files + 1
    return files

# Loopum í gegnum allar skrár til að finna bestu mynd
def finna_besta():
    g = open('gudskrar.txt', 'a')
    for filename in os.listdir(directory):
        if filename.endswith('.csv'):
            dat = np.loadtxt(filename, delimiter = ',', skiprows = 2)
            t = dat[:,0]
            VR = dat[:,1] - Vz
            I = (1/R)*dat[:,1] - Iz
            fig1, ax1 = plot.subplots()
            ax1.plot(t, I)
            #Plottar væntigildi
            for n in range(1,24):
            #    I = (V0/R)/(1 + (12.9e3/R)/n)
            #    I = (V0)/(1 + (12.9e3/R)/n)
                In = (V0)/(R + (12.9e3/n))
                ax1.plot([min(t), max(t)], [In, In])
            ax1.set(xlabel = 'Tími [s]', ylabel = 'Straumur [A]')
            ax1.set_ylim([0, (V0)/(R + 12.9e3/24)])
            fig1.show()
            print(filename)
            x = input()
            if x == 'y':
                g.write(filename + '\n')
            plot.close(fig1)
#finna_besta()
# Opnum gagnaskrá til að vinna með
filename = "scope_7.csv"
data = np.loadtxt(filename, delimiter = ',', skiprows = 2)
t = data[:,0]
VR = data[:,1] - Vz
I = (1/R)*data[:,1] - Iz
# Plottum straum sem fall af tíma ásamt væntigildum
fig1, ax = plot.subplots()
ax.plot(t*1e6, I*1e6)
ax.set(xlabel = 'Tími $T\\,[\\si{\\micro\\second}]$', ylabel = 'Straumur $I\\,[\\si{\\micro\\ampere}]$')
ax.set_ylim([0,65])
ax.set_xlim([-20,-2.5])
for n in range(1,24):
#    I = (V0/R)/(1 + (12.9e3/R)/n)
#    I = (V0)/(1 + (12.9e3/R)/n)
    In = (V0)/(R + (12.9e3/n))
    ax.plot([min(t*1e6), max(t*1e6)], [In*1e6, In*1e6])

#fig1.savefig('scope_49-tI_KOPAR.pdf')
#fig1.show()

# Plottum leiðni sem fall af tíma ásamt væntigildum
G = np.divide(np.divide(VR, R),(V0-VR))

fig2, bx = plot.subplots()
bx.plot(t*1e6, np.divide(G,G0), 'b', Linewidth = 1)
bx.set(ylabel = u'Leiðni $\\sigma\\,[2e^2/h]$', xlabel = 'Tími $T\\,[\\si{\\micro\\second}]$')
bx.set_ylim([0, 23])
bx.set_xlim([-20, -2.5])

for n in range(1,24):
    bx.plot([min(t*1e6), max(t*1e6)], [n, n])
#fig2.savefig('scope_49-tC_KOPAR.pdf')
#fig2.show()

# Sameinum öll mæligögnin í eitt fylki
TOT = np.empty(0)
dats = open('gudskrar.txt')
for filename in dats:
    filename = filename.rstrip('\n')
    data = np.loadtxt(filename, delimiter = ',', skiprows = 2)[:,1]
    TOT = np.append(TOT, data)
# Munum að draga Vz frá gildunum
TOT = TOT - Vz

# Plottum stuðlarit af straumi ásamt væntigildum
fig3, cx = plot.subplots()
data1 = np.divide(TOT,R)
cx.hist(data1*1e6, np.arange(-1e1, 1e2 + BW*1e6, BW*1e6))

for n in range(1,24):
    I = 1e6*(V0/R)/(1 + (12.9e3/R)/n)
    cx.plot([I,I],[0,300], Linewidth = 1)
cx.plot([0,0], [0,300], Linewidth = 1)

cx.set_xlim([-0.5e1, 5e1])
cx.set(xlabel = u'Straumur $I\\,[\\si{\\micro\\ampere}]$', ylabel = 'Atvik')
fig3.savefig('TOThistoI_TOTAL.pdf')
fig3.show()

# Plottum stuðlarit af leiðni ásamt væntigildum
TOT_G = np.divide(np.divide(TOT,R), (V0 - TOT))

fig4, dx = plot.subplots()
f4 = plot.figure(4)
data2 = np.divide(TOT_G, G0)
dx.hist(data2, np.arange(-1, 12 + 0.1, 0.1))
dx.set(xlabel = u'Leiðni $\\sigma\\,[2e^2/h]$', ylabel = 'Atvik')
dx.set_xlim([-0.5, 8])

for n in range(24):
    dx.plot([n, n], [0, 200], Linewidth = 1) 

fig4.savefig('TOThistoC_TOTAL.pdf')
fig4.show()
input()


