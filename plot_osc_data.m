% Unnar Arnalds

clear all;
close all;

% power supply voltage
V0 = 0.1011;
% Resistor value
R=1000;
% offset voltage on the scope, often different between measurements
Vz=0;%-0.7e-3;
% offset current calculated form the resistance
Iz=Vz/R; 

% Load a file to plot data from
filename = "scope_4.csv";
Ga = load(filename);
t = Ga(:,1);
VR = Ga(:,2)-Vz; 
I = (1/R)*Ga(:,2)-Iz;
G = (VR./R)./(V0-VR);

figure
plot(t,I);
set(gca,'FontName','helvetica','FontSize',16);
xlabel('Time [s]');
ylabel('Current [A]');
hold on
% Add expected lines
for n=0:1:24;
%    I = (V0/R)/(1+(12.9E3/R)/n);
%    I = (V0)/(1+(12.9E3/R)/n);
    In = (V0)/(R+(12.9E3/n));
    plot([min(t),max(t)],[In,In]);
end
axis([min(t) max(t) -2e-6 8e-5])

% Plot using determined conductance values of the nanowire 2e^2/h
G0 = 7.7498e-5;
figure
plot(t,G/G0,'b','Linewidth',1);
set(gca,'FontName','helvetica','FontSize',18)
xlabel('Time [s]');
ylabel('Conductance [2e^2/h]');
hold on;
for n=0:1:24;
    plot([min(t),max(t)],[n,n]);
end
axis([min(t) max(t) -1 20])

% HISTOGRAM load all the data
filename = 'scope_0.csv';
load(filename);
filename = 'scope_1.csv';
load(filename);
filename = 'scope_2.csv';
load(filename);
filename = 'scope_3.csv';
load(filename);
filename = 'scope_4.csv';
load(filename);
filename = 'scope_5.csv';
load(filename);
filename = 'scope_6.csv';
load(filename);
filename = 'scope_7.csv';
load(filename);
filename = 'scope_8.csv';
load(filename);
filename = 'scope_9.csv';
load(filename);
filename = 'scope_10.csv';
load(filename);
filename = 'scope_11.csv';
load(filename);

% all current values appended together
% remember to withdraw Vz
TOT = -Vz+[ scope_0(:,2); scope_1(:,2);scope_2(:,2);scope_3(:,2);scope_4(:,2);scope_5(:,2);scope_6(:,2);;scope_7(:,2);scope_8(:,2);scope_9(:,2);scope_10(:,2);scope_11(:,2) ] ;

BW = 8E-7;
figure
histogram(TOT/R,'BinWidth',BW,'BinLimits',[-1e-5,1e-4]);
set(gca,'FontName','helvetica','FontSize',16)
xlabel('Current [A]');
ylabel('Occurrence [Counts]');
hold on
% Add expected lines
for n=0:1:24;
    I = (V0/R)/(1+(12.9E3/R)/n);
    plot([I,I],[0,100],'LineWidth',2);
end
axis([-0.5e-5 5e-5 0 200])

% Histogram for conductance values
TOT_G = (TOT/R)./(V0-TOT);
figure
histogram(TOT_G/G0,'BinWidth',0.1,'BinLimits',[-1,12]);
set(gca,'FontName','helvetica','FontSize',16)
xlabel('Conductance [2e^2/h]');
ylabel('Occurrence [Counts]');
hold on
hold on
axis([-0.5 8 0 200])
for n=0:1:24;
    plot([n n],[0,100],'LineWidth',2);
end



