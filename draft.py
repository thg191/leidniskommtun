# Unnar Arnalds

import numpy as np
import matplotlib
import matplotlib.pyplot as plot
import os

# power supply voltage
V0 = 0.1011
# Resistor value
R = 1000
# offset voltage on the scope, often different between measurements
Vz = 0#-0.7e-3
# offset current calculated form the resistance
Iz = Vz/R 

# Load a file to plot data from
file1 = open("scope_4.csv")
lines = file1.readlines()
lines.rstrip('\n')
t = []
VR = []
I=[]
for x in lines:
    t.append(x.split(',')[0])
    VR.append(x.split(',')[1] - Vz)
    I.append((1/R)*x.split(',')[1] - Iz)

G = np.divide(np.divide(VR,R),(V0-VR))

fig, ax = plt.subplots()
ax.plot(t, I)
#set(gca,'FontName','helvetica','FontSize',16)
ax.set(xlabel='Time [s]', ylabel='Current [A]')

# Add expected lines
for n in range(1,24):
#    I = (V0/R)/(1+(12.9E3/R)/n)
#    I = (V0)/(1+(12.9E3/R)/n)
    In = (V0)/(R+(12.9E3/n))
    ax.plot([min(t),max(t)],[In,In])
#axis([min(t) max(t) -2e-6 8e-5])

# Plot using determined conductance values of the nanowire 2e^2/h
G0 = 7.7498e-5
fig, bx = plt.subplots()
bx.plot(t,np.divide(G,G0),'b',Linewidth=1)
#set(gca,'FontName','helvetica','FontSize',18)
bx.set(xlabel='Time [s]', ylabel='Conductance[2e^2/h]')
for n in range(24):
    bx.plot([min(t),max(t)],[n,n])
#axis([min(t) max(t) -1 20])

# HISTOGRAM load all the data
TOT = []
for filename in os.listdir('/home/thg191/Documents/HI/V2019/Verklegt/Leiðniskömmtun/'):
    if filename.endswith('.csv'):
        scope = open(filename)
        scopelines = scope.readlines()
        for line in scopelines:
            line.rstrip('\n')
            line = [float(i) for i in line.split(',')]
            TOT.append(line[1])
TOT = TOT - Vz
# all current values appended together
# remember to withdraw Vz
for n in range(11):
TOT = -Vz+[ scope_0(:,2) scope_1(:,2)scope_2(:,2)scope_3(:,2)scope_4(:,2)scope_5(:,2)scope_6(:,2)scope_7(:,2)scope_8(:,2)scope_9(:,2)scope_10(:,2)scope_11(:,2) ] 

BW = 8E-7
figure
histogram(TOT/R,'BinWidth',BW,'BinLimits',[-1e-5,1e-4])
set(gca,'FontName','helvetica','FontSize',16)
xlabel('Current [A]')
ylabel('Occurrence [Counts]')
hold on
# Add expected lines
for n=0:1:24
    I = (V0/R)/(1+(12.9E3/R)/n)
    plot([I,I],[0,100],'LineWidth',2)
end
axis([-0.5e-5 5e-5 0 200])

# Histogram for conductance values
TOT_G = (TOT/R)./(V0-TOT)
figure
histogram(TOT_G/G0,'BinWidth',0.1,'BinLimits',[-1,12])
set(gca,'FontName','helvetica','FontSize',16)
xlabel('Conductance [2e^2/h]')
ylabel('Occurrence [Counts]')
hold on
hold on
axis([-0.5 8 0 200])
for n=0:1:24
    plot([n n],[0,100],'LineWidth',2)
end



