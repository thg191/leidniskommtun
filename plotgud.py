
#!usr/env/bin python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib
import matplotlib.pyplot as plt

V0 = 0.1011
Vz = 0
R = 1000
Iz = Vz/R

filename = 'gudskrar.txt'
subdir = ''

f = open(filename)
for line in f:
    line = line.rstrip('\n')
    line = subdir + line
    print(line)
    data = np.loadtxt(line, delimiter = ',', skiprows = 2)
    T = data[:,0]
    VR = data[:,1] - Vz
    I = (1/R)*data[:,1] - Iz
    fig1, ax1 = plt.subplots()
    ax1.plot(T, I)
    for n in range(1,24):
        In = (V0)/(R + (12.9e3/n))
        ax1.plot([min(T), max(T)], [In, In])
    print(line)
    fig1.show()
    input()
    plt.close(fig1)
f.close()


